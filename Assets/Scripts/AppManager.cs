﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using System;


public enum AppState
{
	Initial = 0,
	Counting,
	Finished
}


public class AppManager : MonoBehaviour {


    public int Sub7PrayerWaitTime = 20;
    public int DuhrPrayerWaitTime = 15;
    public int AsrPrayerWaitTime = 15;
    public int MaghribPrayerWaitTime = 10;
    public int IshaaPrayerWaitTime = 15;
    public int FinishedStateDuration = 5;


	GameObject GpInitialState;
	GameObject GpCountingState;
	GameObject GpFinishedState;

	Text LblCounter;

	AppState CurrentState;
    GameObject CurrentStateGameObject;

    float CountingStateDurationCounter;
    float FinishedStateDurationCounter;


	void Awake()
	{
        GameObject CnvMain = transform.Find("/CnvMain").gameObject;
        GpInitialState =  CnvMain.transform.Find("GpInitialState").gameObject;
        GpCountingState = CnvMain.transform.Find("GpCountingState").gameObject;
        GpFinishedState = CnvMain.transform.Find("GpFinishedState").gameObject;

        LblCounter = GpCountingState.transform.Find ("LblCounter").gameObject.GetComponent<Text> ();
	}


	void Start()
	{
        Screen.sleepTimeout = SleepTimeout.NeverSleep;

		CurrentState = AppState.Initial;
        CurrentStateGameObject = GpInitialState;
	}
	

	void Update()
	{
        if (CurrentState == AppState.Counting)
        {
            CountingStateDurationCounter -= Time.deltaTime;
            if (CountingStateDurationCounter <= 0)
            {
                LblCounter.text = "00:00";
                SwitchState(AppState.Finished);
            }
            else
            {
                TimeSpan Ts = TimeSpan.FromSeconds(CountingStateDurationCounter);
                LblCounter.text = string.Format("{0:D2}:{1:D2}", (int)Ts.TotalMinutes, Ts.Seconds);
            }
        }
        else if (CurrentState == AppState.Finished)
        {
            FinishedStateDurationCounter -= Time.deltaTime;
            if (FinishedStateDurationCounter <= 0)
            {
                SwitchState(AppState.Initial);
            }
        }
	}




    public void SwitchState(AppState anyState)
    {
        CurrentState = anyState;
        CurrentStateGameObject.SetActive(false);

        switch (anyState)
        {
            case AppState.Initial:
                
                CurrentStateGameObject = GpInitialState;
                break;


            case AppState.Counting:
                
                CurrentStateGameObject = GpCountingState;
                LblCounter.text = string.Format("{0}:00", CountingStateDurationCounter);

                break;


            case AppState.Finished:

                CurrentStateGameObject = GpFinishedState;
                FinishedStateDurationCounter = FinishedStateDuration;
                gameObject.GetComponent<AudioSource>().Play();

                break;

        }

        CurrentStateGameObject.SetActive(true);
    }


	public void OnSub7()
	{
        CountingStateDurationCounter = Sub7PrayerWaitTime * 60;
        SwitchState(AppState.Counting);
	}


	public void OnDuhr()
	{
        CountingStateDurationCounter = DuhrPrayerWaitTime * 60;
        SwitchState(AppState.Counting);
	}


	public void OnAsr()
	{
        CountingStateDurationCounter = AsrPrayerWaitTime * 60;
        SwitchState(AppState.Counting);
	}


	public void OnMaghrib()
	{
        CountingStateDurationCounter = MaghribPrayerWaitTime * 60;
        SwitchState(AppState.Counting);
	}


	public void OnIshaa()
	{
        CountingStateDurationCounter = IshaaPrayerWaitTime * 60;
        SwitchState(AppState.Counting);
	}


    public void CancelTimer()
    {
        SwitchState(AppState.Initial);
    }
}
